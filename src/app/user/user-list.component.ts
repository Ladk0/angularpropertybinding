import {Component, OnInit} from '@angular/core';
import {User} from '../models/user.model';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-user-list',
  template: '<h2>We got {{users.length}} total users</h2>' +
    '<table align="center" style="text-align: left">' +
    '<app-user *ngFor="let user of users"' +
    ' [user]="user"></app-user></table>'
})
export class UserListComponent implements OnInit {
  users: User[];

  constructor(private userService: UserService) {

  }

  ngOnInit() {
    this.userService.getUsers()
      .subscribe(data => {
        this.users = data;
      });
  }
}

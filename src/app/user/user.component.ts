import {Component, Input} from '@angular/core';
import {UserService} from '../services/user.service';
import {User} from '../models/user.model';

@Component({
  selector: 'app-user',
  template: '<tr>' +
    '<td>{{user.id}}</td>' +
    '<td>{{user.firstName}}</td>' +
    '<td>{{user.lastName}}</td>' +
    '<td>{{user.email}}</td>' +
    '</tr><br/>'
})

export class UserComponent {
  @Input() user: User;
  @Input() userId: number;

  constructor(private userService: UserService) {
  }

  getUserById(): void {
    this.userService.getUserById(this.userId);
  }

  createUser(): void {
    this.userService.createUser(this.user);
  }

  deleteUser(): void {
    this.userService.deleteUser(this.user);
  }

  updateUser(): void {
    this.userService.updateUser(this.user);
  }
}
